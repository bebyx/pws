class ArticlesController < ApplicationController
	
	before_action :set_article, only: [:show, :edit, :update, :destroy]

	def index
		@articles = Article.all.order(created_at: :desc).paginate(page: params[:page], per_page: 5)
	end

	def show
		
	end

	def new
		@article = Article.new
	end

	def edit
		
	end

	def create
		@article = Article.new(article_params)

		if @article.save
			redirect_to @article
		else
			render 'new'
		end
	end

	def update
		if @article.update(article_params)
			redirect_to @article
		else
			render 'edit'
		end
	end

	def destroy
		@article.destroy

		redirect_to articles_path
	end

	def set_article
		@article = Article.friendly.find(params[:id])
	end

	private
		def article_params
			params.require(:article).permit(:title, :text)	
		end

end
