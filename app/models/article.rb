require "babosa"

class Article < ApplicationRecord
	has_many :comments, dependent: :destroy
	validates :title, presence: true,
					  length: { minimum: 1}

	extend FriendlyId
  	friendly_id :title, use: :slugged

  	def normalize_friendly_id(input)
    	input.to_s.to_slug.normalize(transliterations: :ukrainian).to_s
  	end

end
