Rails.application.routes.draw do
  root 'static_pages#home'
  get 'hobby', to: 'static_pages#hobby'
  get 'about', to: 'static_pages#about'

  resources :articles do
  	resources :comments
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
