# Personal website project

A Rails project for the 'web 1.0'-style personal website.

The main goal is to build lightweight and minimalist personal website. Static pages should be complemented with dynamic blog section.

Markdown is planned to be added for text editing (for blog post body and comments).

Internationalization (Ukrainian + English) is also planned.

##Specifications

* Ruby version 2.7.0

* Rails version 6.0.2.1